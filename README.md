# LearningML scratch-vm

LearningML is an educative platform intended to teach and learn Machine Learning
(ML) fundamentals through hand-on activities. The full platform is composed of
four parts:

- a web site (https://learnigml.org)
- a Machine Learning editor (https://gitlab.com/learningml/learningml-editor)
- a programming editor, which is a modification of the Scratch project
      (https://scratch.mit.edu/) (this repo is part of it)
- an API server application (TODO: REPO URL HERE).

Scratch is a block based programming platform intended to learn to code. It is 
specially appropiated for beginners and  children. Scratch is, without any doubt,
the leading programming platform in the educational world. In addition, Scratch 
is an open source project, hence we can install an instance as well as study and
modify its code. For that reason, we have chosen Scratch as the programming
platform of LearningML. We have added some new blocks aimed to use the Machine
Learning models built with the [LearningML editor](https://gitlab.com/learningml/learningml-editor).

The oficial code of Scratch is hosted in github (https://github.com/LLK/). The
platform is organized in several projects. We have needed to modify three of 
them:

- [scratch-gui](https://github.com/LLK/scratch-gui)
- [scratch-vm](https://github.com/LLK/scratch-vm)
- [scratch-l10n](https://github.com/LLK/scratch-l10n) 

Our modification are hosted on github, and have been renamed by addig 
`learningml` as prefix:

- [lml-scratch-gui](https://gitlab.com/learningml/lml-scratch-gui)
- [lml-scratch-vm](https://gitlab.com/learningml/lml-scratch-vm)
- [lml-scratch-l10n](https://gitlab.com/learningml/lml-scratch-l10n)

We try to maintain the master branch of these repos in sync with the oficial 
github repos.

# Installing LearningML-Scratch for development

See `README.md` of [lml-scratch-gui](https://gitlab.com/learningml/lml-scratch-gui).


# Building a production release

Run the following command inside `lml-scratch-gui` directory:

    # npm run build

A deployment release of Scratch platform is available inside `build` directory.

# How the new Machine Learning blocks have been added

We have used the extension mechanism available in Scratch to add the new 
Machine Learning (ML) blocks we need to use the ML models built with 
LearningML editor. You can see how to do this in the `README.md` file of
[lml-scratch-gui](https://gitlab.com/learningml/lml-scratch-gui), 
since the extension must be registered in `lml-scratch-gui` code.

However, the code where the new blocks are declared belongs to this project 
(`lml-scratch-vm`). You can find all the extensions code in `src/extensions` directory. In addition finally you must also register the
extension as a builtin extension in the file `/src/extension-support/extension-manager.js` of this project:

    const builtinExtensions = {
        ...
        learningmlTexts: () => require('../extensions/scratch3_learningml-texts'),
    };

Take an existing extension as model or template to know how add new blocks.
You can see that almost all the code where the new blocks are declared, is
writen in a only `index.js` file. Of course you can break the code in several 
files and organize as you want.

# Machine Learnin Block declaration

The new blocks added for using ML models built with LearningML editor, have been
declared in two extensions: `learningmlTexts`, `learningmlImages`.

## LearningML text extension

The blocks for using ML text recognition models are declared in the file 
`src/extensions/scratch3_learningml-texts`. In this file a class named
`Scratch3LearningMLBlocksTexts` is declared. In this class all the blocks
and their corresponding logic are defined.

The attribute `this.brainText` is intended to store the trained neural network
which is comming from LearningML editor, and which is an instance of the 
object `BrainText` (`this.brainText = new BrainText();`). This object is 
defined in the imported module `brain-text`, which have been developed as a
wrapper to the library [`Brain.js`](https://brain.js.org/#/). The latter is a
library intended to build neural networks models in javascript in a very easy
way, and it was selected at the very beginning of this project. 

The blocks are defined in `getInfo()` method, which returns a Javascript object
with all the needed definitions for deploying the blocks in Scratch. Each 
blocks has an attribute named `opcode` which is the name of the function to 
be called when the block is runned. 

- `classifyText` and `confidenceText` are the function called when the 
  classification an confidence blocks are runned. Both of them use the promise 
  `this.getModelFromEasyML()` to return the classification or confidence given 
  by the LearningML model for the text passed as argument.

- `this.getModelFromEasyML()` is responsible of getting the ML model from 
  the item `easyml_model` of the browser localStorage, where LearningML 
  editor has stored a serialized copy of the model.

- `this.sincroLocalModel()`, which is called by `this.getModelFromEasyML()`, is
  responsible of updating the model if needed (for instance when the model is
  modified by `trainAndWait` block), or leaving it as is when no change has been made.

The most important thing to know is that the model was firstly stored in the browser local storage by LearningML editor when the model was built. And that 
any subsequent change made later to the model, eithe by LearningML editor or by 
Scratch code, provokes the updating of the serialized copy stored in local
storage.

## LearningML image extension

The blocks for using ML image recognition models are declared in the file 
`src/extensions/scratch3_learningml_images`. In this file, a class named
`Scratch3LearningMLImagesBlocks` is declared. In this class all the blocks
and their corresponding logic are defined.

The blocks are defined in `getInfo()` method, which returns a Javascript object
with all the needed definitions for deploying the blocks in Scratch. Each 
blocks has an attribute named `opcode` which is the name of the function to 
be called when the block is runned.

LearningML editor uses [Tensorflow.js](https://www.tensorflow.org/js) to build 
image recognition models. In addition a technique known as [transfer learning](https://www.tensorflow.org/js/tutorials/transfer/what_is_transfer_learning) is
used as strategy to build efficient ML models even with little image datasets.
Therefore, classification and confidence blocks of this extension also uses 
transfer learning technique.

In short, with transfer learning a model developed for a task is reused as the
starting point for a model on a second task. There exist open accessible models,
such as [mobilenet](https://arxiv.org/abs/1704.04861) for image recognition, 
which have been pre trained with thousands or even millions of labeled data. 
While these models probably will not fit well with the problem being solved,
some of their internal layers can be used as the input of a second model. As 
this filtered input has been somehow discriminated by the first powerful model,
the second model can be designed to give as output the labels chosen by the 
learner and also have a much simpler architecture



# Original README.md content

## scratch-vm
#### Scratch VM is a library for representing, running, and maintaining the state of computer programs written using [Scratch Blocks](https://github.com/LLK/scratch-blocks).

[![Build Status](https://travis-ci.org/LLK/scratch-vm.svg?branch=develop)](https://travis-ci.org/LLK/scratch-vm)
[![Coverage Status](https://coveralls.io/repos/github/LLK/scratch-vm/badge.svg?branch=develop)](https://coveralls.io/github/LLK/scratch-vm?branch=develop)

## Installation
This requires you to have Git and Node.js installed.

To install as a dependency for your own application:
```bash
npm install scratch-vm
```
To set up a development environment to edit scratch-vm yourself:
```bash
git clone https://github.com/LLK/scratch-vm.git
cd scratch-vm
npm install
```

## Development Server
This requires Node.js to be installed.

For convenience, we've included a development server with the VM. This is sometimes useful when running in an environment that's loading remote resources (e.g., SVGs from the Scratch server). If you would like to use your modified VM with the full Scratch 3.0 GUI, [follow the instructions to link the VM to the GUI](https://github.com/LLK/scratch-gui/wiki/Getting-Started).

## Running the Development Server
Open a Command Prompt or Terminal in the repository and run:
```bash
npm start
```

## Playground
To view the Playground, make sure the dev server's running and go to [http://localhost:8073/playground/](http://localhost:8073/playground/) - you will be directed to the playground, which demonstrates various tools and internal state.

![VM Playground Screenshot](https://i.imgur.com/nOCNqEc.gif)


## Standalone Build
```bash
npm run build
```

```html
<script src="/path/to/dist/web/scratch-vm.js"></script>
<script>
    var vm = new window.VirtualMachine();
    // do things
</script>
```

## How to include in a Node.js App
For an extended setup example, check out the /src/playground directory, which includes a fully running VM instance.
```js
var VirtualMachine = require('scratch-vm');
var vm = new VirtualMachine();

// Block events
Scratch.workspace.addChangeListener(vm.blockListener);

// Run threads
vm.start();
```

## Abstract Syntax Tree

#### Overview
The Virtual Machine constructs and maintains the state of an [Abstract Syntax Tree](https://en.wikipedia.org/wiki/Abstract_syntax_tree) (AST) by listening to events emitted by the [scratch-blocks](https://github.com/LLK/scratch-blocks) workspace via the `blockListener`. Each target (code-running object, for example, a sprite) keeps an AST for its blocks. At any time, the current state of an AST can be viewed by inspecting the `vm.runtime.targets[...].blocks` object.

#### Anatomy of a Block
The VM's block representation contains all the important information for execution and storage. Here's an example representing the "when key pressed" script on a workspace:
```json
{
  "_blocks": {
    "Q]PK~yJ@BTV8Y~FfISeo": {
      "id": "Q]PK~yJ@BTV8Y~FfISeo",
      "opcode": "event_whenkeypressed",
      "inputs": {
      },
      "fields": {
        "KEY_OPTION": {
          "name": "KEY_OPTION",
          "value": "space"
        }
      },
      "next": null,
      "topLevel": true,
      "parent": null,
      "shadow": false,
      "x": -69.333333333333,
      "y": 174
    }
  },
  "_scripts": [
    "Q]PK~yJ@BTV8Y~FfISeo"
  ]
}
```

## Testing
```bash
npm test
```

```bash
npm run coverage
```

## Publishing to GitHub Pages
```bash
npm run deploy
```

This will push the currently built playground to the gh-pages branch of the
currently tracked remote.  If you would like to change where to push to, add
a repo url argument:
```bash
npm run deploy -- -r <your repo url>
```

## Donate
We provide [Scratch](https://scratch.mit.edu) free of charge, and want to keep it that way! Please consider making a [donation](https://secure.donationpay.org/scratchfoundation/) to support our continued engineering, design, community, and resource development efforts. Donations of any size are appreciated. Thank you!
